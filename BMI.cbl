       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI-CALCULATE.
       AUTHOR. SARAWUT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 USER.
          05 BMI            PIC 99V99 VALUE ZEROS.
          05 WEIGHT         PIC 99.
          05 HEIGHT         PIC 999.

       01 LEVEL-BMI         PIC X(15) VALUE SPACES.
          88 LEVEL1                   VALUE "UNDERWEIGHT".
          88 LEVEL2                   VALUE "NORMAL".
          88 LEVEL3                   VALUE "OVERWEIGHT".
          88 LEVEL4                   VALUE "OBESE".
          88 INVALID-INPUT            VALUE "INVALID".

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM USER-INPUT
           PERFORM CALCULATE-BMI
           DISPLAY "User weight = " WEIGHT "kg height = " HEIGHT "cm"
           DISPLAY "User BMI is - " BMI " | " LEVEL-BMI
           GOBACK 
           .
       USER-INPUT.
           DISPLAY "Enter your weight(kg) - " WITH NO ADVANCING 
           ACCEPT WEIGHT
           DISPLAY "Enter your height(cm) - " WITH NO ADVANCING 
           ACCEPT HEIGHT
           .

       CALCULATE-BMI.
           COMPUTE BMI = WEIGHT /((HEIGHT / 100) *(HEIGHT / 100))
           END-COMPUTE 
           IF BMI LESS THAN 0 THEN
              SET INVALID-INPUT TO TRUE
           ELSE 
              IF BMI LESS THAN 18.50
                 SET LEVEL1 TO TRUE
              ELSE
                 IF BMI LESS THAN 25.00
                    SET LEVEL2 TO TRUE
                 ELSE
                    IF BMI LESS THAN OR EQUAL TO 30.00
                       SET LEVEL3 TO TRUE
                    ELSE
                       IF BMI GREATER 30.00
                          SET LEVEL4 TO TRUE
                       END-IF
                    END-IF
           
           .
